from typing import List
import argparse, os
import src.regex2epsafn as regex2epsafn
import src.epsafn2afn as epsafn2afn
import src.epsafn2afd as epsafn2afd
from src.automaton import Automaton
import pygraphviz as pgv

DEFAULT_OUTPUT_PATH = os.path.dirname(os.path.abspath(__file__))+"/lab2ex3/lab2ex3"
OPERATIONS = ['union', 'compl', 'intersec']

def operate(operation: str, regexes: List[str]) -> Automaton:
    if operation == OPERATIONS[1]:
        return complement(regexes[0])
    elif operation == OPERATIONS[0]:
        return union(regexes[0], regexes[1])
    else:
        return intersec(regexes[0], regexes[1])

def complement(regex: str) -> Automaton:
    epsafn_automaton = Automaton.from_regex(regex)

    afd_graph, afd_finals = epsafn2afd.run(epsafn_automaton.graph, epsafn_automaton.finalstates, "a")
    afd_automaton = Automaton(afd_graph, afd_finals, "AFD")
    afd_automaton.complement()

    return afd_automaton

def union(regex1: str, regex2: str) -> Automaton:
    automaton1 = Automaton.from_regex(regex1)
    automaton2 = Automaton.from_regex(regex2)

    return Automaton.union(automaton1, automaton2)

def intersec(regex1: str, regex2: str) -> Automaton:
    epsafn_automaton1 = Automaton.from_regex(regex1)
    afd_graph1, afd_finals1 = epsafn2afd.run(epsafn_automaton1.graph, epsafn_automaton1.finalstates, "a")
    afd_automaton1 = Automaton(afd_graph1, afd_finals1, "AFD")
    
    # afd_automaton1.debug("auto")
    
    epsafn_automaton2 = Automaton.from_regex(regex2)
    afd_graph2, afd_finals2 = epsafn2afd.run(epsafn_automaton2.graph, epsafn_automaton2.finalstates, "a")
    afd_automaton2 = Automaton(afd_graph2, afd_finals2, "AFD")

    # afd_automaton2.debug("auto2")

    return Automaton.intersec(afd_automaton1, afd_automaton2)

def main():
    cmdparser = argparse.ArgumentParser(description="Opera duas linguagens descritas por expressões regulares com união, complemento ou intersecção")
    cmdparser.add_argument('operation',
                        metavar="OPERACAO",
                        type=str,
                        help="Operacao a ser feita entre linguagens (union | compl | intersec)")
    cmdparser.add_argument('regexes',
                        metavar="REGEX",
                        nargs='+',
                        type=str,
                        help="Linguagens a serem operadas em formato REGEX")
    cmdparser.add_argument('--out',
                        metavar="SAIDA",
                        nargs=1,
                        type=str,
                        default=DEFAULT_OUTPUT_PATH,
                        help=f"Caminho do arquivo de saida (default: {DEFAULT_OUTPUT_PATH})")
    args = cmdparser.parse_args()

    if (args.operation not in OPERATIONS 
        or (args.operation == OPERATIONS[1] and len(args.regexes) > 1)):
        cmdparser.print_help()
        exit(1)
    
    if len(args.regexes) > 2:
        cmdparser.print_help()
        exit(2)
    
    automaton = operate(args.operation, args.regexes)

    if not os.path.isdir(os.path.dirname(args.out)):
        os.makedirs(os.path.dirname(args.out))
    
    automaton.graph.write(f"{args.out}.dot")
    visual_graph = pgv.AGraph(f"{args.out}.dot")
    visual_graph.layout("circo")
    visual_graph.draw(f"{args.out}.png")
    


if __name__ == "__main__":
    main()