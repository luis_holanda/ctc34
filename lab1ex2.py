import pygraphviz as pgv
import src.epsafn2afn as epsafn2afn

def main():
    εafn = pgv.AGraph(directed=True, strict=False)
    εafn.add_edges_from([(0, 3), (3, 2), (2, 5), (5, 4), (4, 6), (6, 1)], label=epsafn2afn.EPSILON)
    εafn.add_edges_from([(3, 3)], label="a")
    εafn.add_edges_from([(5, 5)], label="b")
    εafn.add_edges_from([(6, 6)], label="c")

    afn, _ = epsafn2afn.run(εafn, "1")

    afn.write("teste4.dot")


if __name__ == '__main__':
    main()
