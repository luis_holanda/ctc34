import sys, os
from src.regexhelpers import check_alphabet, check_parenthesis
import src.regex2epsafn as regex2epsafn

def print_mismatching(seq: str) -> None:
        print(f"A sequencia {seq} possui parenteses desbalanceados ou vazios.")

def print_usage() -> None:
        print("Usage: lab1ex1 [REGEX] [OUTPUT_PATH]")
        print("\tREGEX é uma sequência de letras, numeros, +, * e parenteses balanceados que")
        print("\t nao contenha ** ou ++. REGEX não pode começar ou acabar com o caractere +.")

def main():
    arguments = sys.argv
    if len(arguments) == 1 or len(arguments) > 3:
        print_usage()
        exit(1)

    input = arguments[1]
    ex1_dirpath = os.path.dirname(os.path.abspath(__file__)) + "/ex1_out"
    if not os.path.isdir(ex1_dirpath):
        os.makedirs(ex1_dirpath)
    
    file_path = arguments[2] if len(arguments) == 3 else ex1_dirpath + "/exercicio1.png"

    if not check_parenthesis(input):
        print_mismatching(input)
        exit(2)

    if not check_alphabet(input):
        print_usage()
        exit(3)
    
    visual_graph, _ = regex2epsafn.run(input)
    visual_graph.layout()
    visual_graph.draw(file_path)


if __name__ == '__main__':
    main()