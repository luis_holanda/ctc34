import pygraphviz as pgv
import exercicio2 as ex2

from collections import defaultdict
from typing import List, Dict, Optional
from pprint import pprint as print

from lab2ex1 import add_edge, unreachable


def new():
    return defaultdict(lambda: {})


def goto(afn: pgv.AGraph, q0: str, a: str) -> Optional[str]:
    for edge in afn.out_edges_iter(q0):
        if a in edge.attr["label"]:
            return edge[1]


def is_final(qs, finals):
    return qs in finals


def init(states, finals):
    D, S = new(), new()
    for j, qj in enumerate(states):
        for qi in states[:j]:
            D[qi][qj] = False
            S[qi][qj] = set()

    for j, qj in enumerate(states):
        for qi in states[:j]:
            if (is_final(qi, finals) and not is_final(qj, finals)
                    or is_final(qj, finals) and not is_final(qi, finals)):
                D[qi][qj] = True

    return D, S


def equivalents(afn: pgv.AGraph, states: str, alpha: str, finals: str):
    D, S = init(states, finals)

    def dist(_qi, _qj):
        D[_qi][_qj] = True

        for _qm, _qn in S[_qi][_qj]:
            dist(_qm, _qn)

    def next_state(_qi, _qj, _a):
        qm, qn = goto(afn, _qi, _a), goto(afn, _qj, _a)

        if None in (qm, qn) or set(qm) == set(qn):
            return None

        return min(qm, qn), max(qm, qn)

    for j, qj in enumerate(states):
        for qi in states[:j]:
            if D[qi][qj]:
                continue

            for a in alpha:
                q = next_state(qi, qj, a)
                if q is None:
                    continue
                q1, q2 = q

                if D[q1][q2]:
                    dist(qi, qj)
                    break
            else:
                for a in alpha:
                    q = next_state(qi, qj, a)
                    if q is None:
                        continue
                    q1, q2 = q
                    S[q1][q2].add((qi, qj))

    parts = set()
    for qi, v in D.items():
        for qj in v:
            if not v[qj]:
                parts.add((qj, qi))

    return parts


def eliminate(afn, equiv, finals, alpha, start):
    state_map = {}
    new_states = set()
    new_finals = []
    new_start = start
    for eq in equiv:
        reprs = eq[0]
        new_states.add(reprs)
        for s in eq:
            state_map[s] = reprs
            if s == start:
                new_start = reprs

    for n in afn.nodes_iter():
        if n not in state_map:
            state_map[n] = n
            new_states.add(n)

    transitions = {}
    for s in new_states:
        transitions[s] = {}
        for a in alpha:
            transitions[s][a] = state_map.get(goto(afn, s, a), 'D')

    transitions['D'] = {}
    transitions['D'].fromkeys(alpha, 'D')

    for un in unreachable(set(transitions.keys()), transitions, new_start):
        del transitions[un]

    for f in finals:
        if f in transitions.keys():
            new_finals.append(f)

    return transitions, new_finals, new_start


def minimize(afn: pgv.AGraph, states: str, alpha: str, finals: str, start: str):
    equiv = equivalents(afn, states, alpha, finals)
    if not equiv:
        return afn

    reduced, fin, st = eliminate(afn, sorted(equiv), finals, alpha, start)

    mafn = pgv.AGraph(directed=True, strict=False)

    for rs in reduced:
        for a, r in reduced[rs].items():
            add_edge(mafn, rs, r, a)

    for f in fin:
        mafn.get_node(f).attr["shape"] = "doublecircle"

    mafn.get_node(st).attr["color"] = "dodgerblue"

    return mafn


def main():
    afd = pgv.AGraph(directed=True)
    afd.read("case3ex1.dot")

    states = []
    finals = []
    for state in afd.nodes():
        states.append(state)
        if state.attr["shape"] == "doublecircle":
            finals.append(state)

    mafd = minimize(afd, sorted(states), "ab", sorted(finals), "0")

    mafd.write("case3ex2.dot")


if __name__ == '__main__':
    main()
