from typing import List, Iterable

import pygraphviz as pgv

from exercicio2 import remove_ε_transitions, label_of


def valid_substrings(q0: str, finals: str,
                     afn: pgv.AGraph, string: str) -> Iterable[str]:
    def valid_substring(q: str, string: str) -> bool:
        for qi in afn.out_neighbors(q):
            if not pass_by(string[0], afn.get_edge(q, qi)):
                continue

            if qi in finals and len(string) == 1:
                return True
            elif len(string) > 1 and valid_substring(qi, string[1:]):
                    return True

        return False

    for substring in generate_substrings(string):
        if valid_substring(q0, substring):
            yield substring


def generate_substrings(string: str) -> Iterable[str]:
    for l in range(len(string), 0, -1):
        for i in range(l):
            yield string[i:l]


def pass_by(char: str, edge: pgv.Edge) -> bool:
    return char in label_of(edge).split(",")


def main():
    εafn = pgv.AGraph(directed=True, strict=False)
    εafn.add_edges_from([(0, 3), (3, 2), (2, 5), (5, 4), (4, 6), (6, 1)], label="ε")
    εafn.add_edges_from([(3, 3)], label="a")
    εafn.add_edges_from([(5, 5)], label="b")
    εafn.add_edges_from([(6, 6)], label="c")

    afn, finals = remove_ε_transitions(εafn, "1")

    for valid in valid_substrings("0", finals, afn, "abacabc"):
        print("Valid:", valid)


if __name__ == '__main__':
    main()
