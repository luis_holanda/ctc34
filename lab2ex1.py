import pygraphviz as pgv

from collections import defaultdict
from pprint import pprint as print

from exercicio2 import remove_ε_transitions


def edges(afn: pgv.AGraph, q0, a):
    for e in afn.out_edges_iter(q0):
        if a in e.attr["label"]:
            yield e[1]


def add_edge(afd: pgv.AGraph, q0: str, qf: str, a: str):
    if not afd.has_edge(q0, qf):
        key = a
    else:
        keys = set(afd.get_edge(q0, qf).key.split(","))
        keys.add(a)
        afd.remove_edge(q0, qf)
        key = ",".join(keys)

    afd.add_edge(q0, qf, key=key, label=key)


def partitions(collection):
    if len(collection) == 1:
        yield set(collection)
        return

    first = collection[0]
    yield {first}
    for smaller in partitions(collection[1:]):
        yield smaller
        yield {first} | smaller


def unreachable(states: set, F, start: str):
    reach = {start}
    new = {start}

    while new:
        temp = set()
        for q in new:
            for c in F[q].values():
                temp.add(c)
        new = temp - reach
        reach |= new

    return states - reach


def convert(afn: pgv.AGraph, states: str, start: str, finals: str, alpha: str):
    dstates = list(states)

    dtrans = {}

    for R in dstates:
        dtrans[R] = {}
        for a in alpha:
            es = set()
            for r in R:
                es |= set(edges(afn, r, a))
            es = "".join(sorted(es))

            if es:
                dtrans[R][a] = es

                if es not in dstates:
                    dstates.append(es)
            else:
                dtrans[R][a] = "D"

    dtrans["D"] = {}
    for a in alpha:
        dtrans["D"][a] = "D"

    for un in unreachable(set(dtrans.keys()), dtrans, start):
        del dtrans[un]

    afd = pgv.AGraph(directed=True)

    for q0, f in dtrans.items():
        for a, qf in f.items():
            add_edge(afd, q0, qf, a)

    for a in alpha:
        for q in afd.nodes():
            for e in afd.out_edges(q):
                if a in e.key:
                    break

            if any(f in q for f in finals):
                q.attr["shape"] = "doublecircle"

    return afd


def main():
    afn = pgv.AGraph(directed=True, strict=False)
    afn.read("case4.dot")

    finals = [n for n in afn.nodes_iter() if n.attr["shape"] == "doublecircle"]
    nodes = afn.nodes()

    afd = convert(afn, nodes, "0", finals, "abc")

    afd.write("case4ex1.dot")


if __name__ == '__main__':
    main()
