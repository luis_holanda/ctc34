def crop_parenthesis(sequence: str) -> str:
    parenthesis = ['(', ')']
    stack = []
    level = [-1] * len(sequence)
    indexes = [-1] * len(sequence)
    for i in range(len(sequence)):
        c = sequence[i]
        if not c in parenthesis:
            continue
        if c == parenthesis[0]:
            level[i] = len(stack)
            stack.append(i)
        elif len(stack) == 0:
            raise ValueError
        else:
            level[i] = level[stack[-1]]
            indexes[stack[-1]] = i
            indexes[i] = stack[-1]
            stack.pop()
    
    if len(stack) > 0:
        raise ValueError

    l, r = 0, len(sequence)-1
    while l < r and sequence[l] == '(' and sequence[r] == ')' and indexes[l] == r:
        l = l + 1
        r = r - 1
    return sequence[l:r+1]

def check_alphabet(sequence: str) -> bool:
    allowed = ['+','*','(',')']
    previous = ''
    for i in range(0,len(sequence)):
        c = sequence[i]
        if not c.isalnum():
            if not c in allowed:
                # Unallowed character
                return False
            if (c == '+' or c == '*') and c == previous:
                # ++ or **
                return False
            #if (c == '+' and ((i+1 < len(sequence) and not sequence[i+1].isalnum())
            #                or (i-1 >= 0 and sequence[i-1] == '(')
            #                or i == 0)):
                # Union missing one paramenter
            #    print(i+1 < len(sequence) and not sequence[i+1].isalnum())
            #    return False
        previous = c
    return len(sequence) == 0 or (sequence[-1] != '+')


def check_parenthesis(sequence: str) -> bool:
    stacksize = 0
    parenthesis = ['(', ')']
    for i in range(0,len(sequence)):
        c = sequence[i]
        if not c in parenthesis:
            continue
        if c == parenthesis[0]:
            if i+1<len(sequence) and sequence[i+1] == parenthesis[1]:
                # Parenthesis with no content
                return False
            stacksize = stacksize + 1
        elif stacksize == 0:
            # More closing parenthesis than opening ones
            return False
        else:
            stacksize = stacksize - 1
    # There should not be more opening parenthesis than closing ones
    return stacksize == 0