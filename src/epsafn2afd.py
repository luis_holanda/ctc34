from typing import Tuple, Set, List
import src.epsafn2afn as epsafn2afn
from src.lab2ex1 import convert
import pygraphviz as pgv

def run(epsafn_graph: pgv.AGraph, epsafn_finalstates: Set[str], alphabet: List[str]) -> Tuple[pgv.AGraph, Set[str]]:    
    afngraph, afnfinals = epsafn2afn.run(epsafn_graph, epsafn_finalstates)

    ## DEBUG
    # out = "semiresult_afn"
    # afngraph.write(f"{out}.dot")
    # g = pgv.AGraph(f"{out}.dot")
    # g.layout("circo")
    # g.draw(f"{out}.png")

    afdgraph, afdfinals = convert(afngraph, afngraph.nodes(), '0', list(afnfinals), alphabet)
    return afdgraph, afdfinals