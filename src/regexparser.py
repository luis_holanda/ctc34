from typing import Tuple, List, Union, Set
import pygraphviz as pgv
from abc import ABC, abstractmethod
from src.regexhelpers import crop_parenthesis

EPSILON_TRANSITION_STR = '&'

class ParsingNode:
    def __init__(self, id: int, final:bool =False) -> None:
        self.id = id
        self.transitions = []
        self.final = final
    
    def add_transition(self, node: 'ParsingNode', expression: str) -> Tuple['ParsingNode', str]:
        self.transitions.append((node,expression))
        return self.transitions[-1]
    
    def __str__(self):
            return str(self.id)


class ParsingGraph:
    def __init__(self) -> None:
        self.root = None
        self.nodes = []
        self.node_count = 0
    
    def belongs(self, node: ParsingNode) -> bool:
        return node.id >= 0 and node.id < self.node_count
    
    def create_node(self, final=False) -> ParsingNode:
        self.nodes.append(ParsingNode(self.node_count, final))
        self.node_count = self.node_count + 1
        return self.nodes[-1]
    
    def link(self, node1: ParsingNode, node2: ParsingNode, expression: str) -> None:
        if node1 is None or node2 is None or expression is None:
            raise ValueError   
        if not self.belongs(node1) or not self.belongs(node2):
            raise ValueError
        
        node1.add_transition(node2, expression)
    
    def gen_visual_graph(self) -> Tuple[pgv.AGraph, Set[str]]:
        g = pgv.AGraph(strict=False, directed=True)
        finals = set()
        visited = [False] * self.node_count
        self.__dfs(self.root, visited, g, finals)
        
        return g, finals

    def __dfs(self, current: ParsingNode, history: List[bool], visualg: pgv.AGraph, finalstates: Set[str]) -> None:
        history[current.id] = True
        if current.final:
            visualg.add_node(str(current), shape='doublecircle')
            finalstates.add(str(current))
        else:
            visualg.add_node(str(current), shape='circle')
        for n in current.transitions:
            l = crop_parenthesis(n[1])
            visualg.add_edge(str(current), str(n[0]), l, label=l)
            if not history[n[0].id]:
                self.__dfs(n[0], history, visualg, finalstates)


class RegLang(ABC):
    def __init__(self, expression: str) -> None:
        self.expression = expression

    def __str__(self):
        return self.expression
    
    @abstractmethod
    def decompose(self, graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode) -> List[Tuple[ParsingNode, ParsingNode, str]]:
        pass
    
    
class LangAtom(RegLang):
    def __init__(self, char: str) -> None:
        super().__init__(f"({char})")
    
    def decompose(self,  graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode) -> List[Tuple[ParsingNode, ParsingNode, str]]:
        start_node.add_transition(end_node, self.expression)
        return []
    
    @classmethod
    def analyze(self, regex: str) -> Union['LangAtom', None]:
        cropped = crop_parenthesis(regex)
        return LangAtom(cropped) if len(cropped) == 1 and (cropped[0].isalnum() or cropped[0] == EPSILON_TRANSITION_STR) else None
        

class LangUnion(RegLang):
    def __init__(self, regex1: str, regex2: str) -> None:
        super().__init__(f"({regex1})+({regex2})")
        self.regex1 = regex1
        self.regex2 = regex2
    
    def decompose(self,  graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode) -> List[Tuple[ParsingNode, ParsingNode, str]]:
        return [ 
            (start_node, end_node, self.regex1),
            (start_node, end_node, self.regex2)
        ]
    
    @classmethod
    def analyze(self, regex: str) -> Union['LangUnion', None]:
        cropped = crop_parenthesis(regex)
        parenthesis = ['(', ')']
        level = 0
        for i in range(0,len(cropped)):
            c = cropped[i]
            if c == parenthesis[0]:
                level = level + 1
            elif c == parenthesis[-1]:
                level = level - 1
            elif c == '+' and level == 0:
                return LangUnion(cropped[:i], cropped[i+1:])
        return None


class LangConcat(RegLang):
    def __init__(self, regex1: str, regex2: str) -> None:
        super().__init__(f"({regex1})({regex2})")
        self.regex1 = regex1
        self.regex2 = regex2
    
    def decompose(self,  graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode) -> List[Tuple[ParsingNode, ParsingNode, str]]:
        intermediary_node = graph.create_node()
        return [
            (start_node, intermediary_node, self.regex1),
            (intermediary_node, end_node, self.regex2)
        ]

    @classmethod
    def analyze(self, regex: str) -> Union['LangConcat', None]:
        cropped = crop_parenthesis(regex)
        
        # (SimpleKleeneStar)(SomethingElse)
        if len(cropped) > 2 and cropped[0].isalnum() and cropped[1] == '*':
            return LangConcat(cropped[:2], cropped[2:])
        
        # (Atomic)(SomethingElse)
        if len(cropped) > 1 and cropped[0].isalnum() and cropped[1] != '*':
            return LangConcat(cropped[0], cropped[1:])

        parenthesis = ['(', ')']
        level = 0
        has_nested_level = False
        for i in range(0,len(cropped)):
            c = cropped[i]
            if c == parenthesis[0]:
                level = level + 1
                has_nested_level = True
            elif c == parenthesis[-1]:
                level = level - 1
            # (ComposeKleeneStar)(SomethingElse)
            if has_nested_level and level == 0 and i+2<len(cropped) and cropped[i+1] == '*':
                return LangConcat(cropped[:i+2], cropped[i+2:])
            # (Something)(SomethingElse)
            if has_nested_level and level == 0 and i+1<len(cropped) and cropped[i+1] != '*':
                return LangConcat(cropped[1:i], cropped[i+1:])

        return None


class KleeneStar(RegLang):

    def __init__(self, regex: RegLang) -> None:
        super().__init__(f"({regex})*")
        self.regex = regex
    
    def decompose(self,  graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode) -> List[Tuple[ParsingNode, ParsingNode, str]]:
        intermediary_node = graph.create_node()
        return [
            (start_node, intermediary_node, EPSILON_TRANSITION_STR),
            (intermediary_node, intermediary_node, self.regex),
            (intermediary_node, end_node, EPSILON_TRANSITION_STR)
        ]
    
    @classmethod
    def analyze(self, regex: str) -> Union['KleeneStar', None]:
        cropped = crop_parenthesis(regex)
        if len(cropped) == 2 and cropped[1] == '*':
            return KleeneStar(cropped[0])
        
        parenthesis = ['(', ')']
        stack = []
        level = [-1] * len(regex)
        indexes = [-1] * len(regex)
        for i in range(len(regex)):
            c = regex[i]
            if not c in parenthesis:
                continue
            if c == parenthesis[0]:
                level[i] = len(stack)
                stack.append(i)
            elif len(stack) == 0:
                raise ValueError
            else:
                level[i] = level[stack[-1]]
                indexes[stack[-1]] = i
                indexes[i] = stack[-1]
                stack.pop()
        
        if (len(cropped) > 3 
           and cropped[0] == '(' 
           and cropped[-2] == ')' 
           and cropped[-1] == '*'
           and indexes[-2] == 0):
            return KleeneStar(cropped[1:len(cropped)-2])
        
        return None


def __recur_parse(graph: ParsingGraph, start_node: ParsingNode, end_node: ParsingNode, expression: RegLang) -> None:
    reglang = None
    rules = [LangAtom, LangUnion, LangConcat, KleeneStar]
    for rule in rules:
        reglang = rule.analyze(expression)
        if reglang is not None:
            break
        
    if reglang is None:
        raise ValueError

    for new_start_node, new_end_node, new_expression in reglang.decompose(graph, start_node, end_node):
        __recur_parse(graph, new_start_node, new_end_node, new_expression)

def parse(sequence: str) -> ParsingGraph:
        graph = ParsingGraph()
        graph.root = graph.create_node()
        final = graph.create_node(True)

        if len(sequence) == 0:
            graph.link(graph.root, final, EPSILON_TRANSITION_STR)
        else:
            __recur_parse(graph, graph.root, final, sequence)

        return graph

if __name__ == '__main__':
    test_analyzers()

def test_analyzers():
    print("LangAtom:")
    print(str(LangAtom.analyze('a')))
    print(str(LangAtom.analyze('1')))
    print(str(LangConcat.analyze('')))
    print(str(LangAtom.analyze('(a)*')))
    print(str(LangAtom.analyze('abc')))

    print("\nLangUnion:")
    print(str(LangUnion.analyze('a+b')))
    print(str(LangUnion.analyze('(a+b)')))
    print(str(LangUnion.analyze('ab(a+b)')))
    print(str(LangUnion.analyze('as(ab)+b+c')))
    print(str(LangUnion.analyze('as(a+b)+b+c')))
    print(str(LangUnion.analyze('(a+b)(b+c)')))

    print("\nLangConcat:")
    print(str(LangConcat.analyze('a')))
    print(str(LangConcat.analyze('ab')))
    print(str(LangConcat.analyze('abc')))
    print(str(LangConcat.analyze('(a)')))
    print(str(LangConcat.analyze('(a)(b)')))
    print(str(LangConcat.analyze('a(bc)')))
    print(str(LangConcat.analyze('(a)bc')))
    print(str(LangConcat.analyze('a(b)c')))
    print(str(LangConcat.analyze('(abc)*a')))
    print(str(LangConcat.analyze('a(abc)*')))
    print(str(LangConcat.analyze('ab*')))
    print(str(LangConcat.analyze('ab*c')))
    print(str(LangConcat.analyze('a*bc')))

    print("\nKleeneStar:")
    print(str(KleeneStar.analyze('a*')))
    print(str(KleeneStar.analyze('(bba)*')))
    print(str(KleeneStar.analyze('(as+a)*')))
    print(str(KleeneStar.analyze('as+a*')))
    print(str(KleeneStar.analyze('(ad)as+a(ab)*')))


