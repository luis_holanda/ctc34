from typing import Set, Tuple
import pygraphviz as pgv
import src.regex2epsafn as regex2epsafn
import src.epsafn2afn as epsafn2afn
import src.epsafn2afd as epsafn2afd

EPSILON = '&'

def normalize_ids(automaton: 'Automaton', refautomaton: 'Automaton', begin: int) -> None:
    ''' Fill automaton with refautomaton but with normalized nodes starting from begin'''
    count = begin
    translation = {}
    for edge in refautomaton.graph.edges(keys=True):
        node1 = edge[0]
        node2 = edge[1]
        
        if translation.get(node1) is None:
            automaton.add_node(count, node1 in refautomaton.finalstates)
            translation[node1] = count
            count = count + 1
        if translation.get(node2) is None:
            automaton.add_node(count, node2 in refautomaton.finalstates)
            translation[node2] = count
            count = count + 1

        automaton.graph.add_edge(translation[node1], translation[node2], edge[2], label=edge[2])
    

def normalize_automatons_ids(automaton1: 'Automaton', automaton2: 'Automaton') -> Tuple['Automaton', 'Automaton']:
    ''' Changes automata nodes so there are no conflicting ids and ids start at 1'''
    new_automaton1 = Automaton(pgv.AGraph(strict=False, directed=True), set(), Automaton.EPS_NDFA)
    new_automaton2 = Automaton(pgv.AGraph(strict=False, directed=True), set(), Automaton.EPS_NDFA)

    normalize_ids(new_automaton1, automaton1, 1)
    normalize_ids(new_automaton2, automaton2, len(automaton1.graph.nodes())+1)
    
    return new_automaton1, new_automaton2


def merge(automaton1: 'Automaton', automaton2: 'Automaton') -> None:
    ''' Merges second automaton into the first '''
    for edge in automaton2.graph.edges(keys=True):
        node1 = edge[0]
        node2 = edge[1]
        if not automaton1.graph.has_node(node1):
            automaton1.add_node(node1, node1 in automaton2.finalstates)
        if not automaton1.graph.has_node(node2):
            automaton1.add_node(node2, node2 in automaton2.finalstates)
        automaton1.graph.add_edge(node1, node2, edge[2], label=edge[2])


class Automaton:
    EPS_NDFA = "EPSNDFA"
    NDFA = "NDFA"
    DFA = "DFA"

    def __init__(self, graph: pgv.AGraph, finalstates: Set[str], representation: str) -> None:
        self.graph = graph
        self.representation = representation
        self.finalstates = finalstates
    
    def add_node(self, node: str, final: bool) -> None:
        if self.graph.has_node(node):
            return

        if final:
            self.finalstates.add(str(node))
            self.graph.add_node(node, shape='doublecircle')
        else:
            self.graph.add_node(node, shape='circle')
    
    def debug(self, out: str) -> None:
        self.graph.write(f"{out}.dot")
        g = pgv.AGraph(f"{out}.dot")
        g.layout("circo")
        g.draw(f"{out}.png")
        

    @classmethod
    def from_regex(cls, regex: str) -> 'Automaton':
        graph, finalstates = regex2epsafn.run(regex)
        return Automaton(graph, finalstates, Automaton.EPS_NDFA)
    
    def complement(self) -> None:
        new_finalstates = set()

        for node in self.graph.nodes():
            if not str(node) in self.finalstates:
                new_finalstates.add(str(node))
    
        self.finalstates = new_finalstates
        new_automaton = Automaton(pgv.AGraph(strict=False, directed=True), set(), self.representation)
        merge(new_automaton, self)

        self.graph = new_automaton.graph
    
    @classmethod
    def union(cls, automaton1: 'Automaton', automaton2: 'Automaton') -> 'Automaton':
        ''' OBS: automata must be in EPS_NDFA representation '''
        total_nodes = len(automaton1.graph.nodes()) + len(automaton2.graph.nodes())
        new_automaton1, new_automaton2 = normalize_automatons_ids(automaton1, automaton2)
        
        merge(new_automaton1, new_automaton2)

        new_automaton1.add_node(0, False)

        new_automaton1.graph.add_edge(0, 1, label=EPSILON)
        new_automaton1.graph.add_edge(0, len(automaton1.graph.nodes())+1, label=EPSILON)

        return new_automaton1
    
    @classmethod
    def intersec(cls, automaton1: 'Automaton', automaton2: 'Automaton') -> 'Automaton':
        ''' OBS: automata must be in FDA representation '''
        automaton1.complement()
        # automaton1.debug("auto_compl")

        automaton2.complement()
        # automaton2.debug("auto2_compl")

        total_nodes = len(automaton1.graph.nodes()) + len(automaton2.graph.nodes())
        new_automaton1, new_automaton2 = normalize_automatons_ids(automaton1, automaton2)
        
        merge(new_automaton1, new_automaton2)

        new_automaton1.add_node(0, False)
        new_automaton1.graph.add_edge(0, 1, label=EPSILON)
        new_automaton1.graph.add_edge(0, len(automaton1.graph.nodes())+1, label=EPSILON)

        # new_automaton1.debug("union_compl")

        automaton_graph, automaton_finals = epsafn2afd.run(new_automaton1.graph, new_automaton1.finalstates, "a")
        automaton = Automaton(automaton_graph, automaton_finals, "AFD")
        #automaton.debug("semiresult")

        automaton.complement()
        #automaton.debug("result")

        return automaton