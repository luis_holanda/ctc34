import pygraphviz as pgv
from functools import reduce
from typing import Callable, List, Set, Dict, FrozenSet, Tuple, Iterator
from collections import defaultdict

EPSILON = "&"

Transitions = Dict[pgv.Node, Set[pgv.Node]]


def run(εafn: pgv.AGraph, finals: str) -> Tuple[pgv.AGraph, Set[str]]:
    """Remove any epsilon-transitions in the given afn."""
    clojures = εClojures(εafn)
    clojures.calculate()
    return εafn_to_afn(clojures, finals)


class εClojures:
    def __init__(self, afn: pgv.AGraph) -> None:
        self.__ε_clojures: Transitions = {}
        self.__ε_transitions: Transitions = {}
        self.afn: pgv.AGraph = afn

    def __transitions(self, state: pgv.Node) -> Set[pgv.Node]:
        if state in self.__ε_transitions:
            return self.__ε_transitions[state]

        transitions = set(state)
        for edge in self.afn.out_edges(state):
            if label_of(edge) == EPSILON:
                transitions.add(edge[1])
        self.__ε_transitions[state] = transitions

        return transitions

    def calculate(self) -> None:
        for node in self.afn.nodes_iter():
            self.__clojure_of(node)

    def __call__(self, state: pgv.Node) -> Set[pgv.Node]:
        if state in self.__ε_clojures:
            return self.__ε_clojures.get(state)
        else:
            return self.__clojure_of(state)

    def __clojure_of(self, state: pgv.Node) -> Set[pgv.Node]:
        clojure: Set[pgv.Node] = self.__transitions(state)

        added = clojure.copy()
        while len(added):
            add_next = set()

            for node in added:
                add_next |= self.__transitions(node)

            added = add_next - clojure
            clojure |= added

        self.__ε_clojures[state] = clojure

        return clojure


def εafn_to_afn(clojures: εClojures, finals: str) -> Tuple[pgv.AGraph, Set[str]]:
    graph = defaultdict(lambda: defaultdict(lambda: set()))
    εafn = clojures.afn

    def translate_graph() -> Tuple[pgv.AGraph, Set[str]]:
        afn = pgv.AGraph(directed=True, strict=False)
        afn.node_attr.update(shape="circle")
        for x, edges in graph.items():
            for y, ks in edges.items():
                l = ",".join(sorted(ks))
                afn.add_edge(x, y, l, label=l)

        afn_finals = set()
        for x in afn.nodes_iter():
            for y in clojures(x):
                if y in finals:
                    afn_finals.add(x)
                    x.attr["shape"] = "doublecircle"
        return afn, afn_finals

    for X in εafn.nodes_iter():
        in_edges = filter_ε_transitions(εafn.in_edges_iter(X))
        a_to_y_edges = [(e[0], label_of(e)) for e in in_edges]

        for Y in clojures(X):
            for A, label in a_to_y_edges:
                graph[A][Y].add(label)

            out_edges = filter_ε_transitions(εafn.out_edges_iter(Y))
            for e in out_edges:
                A, label = e[1], label_of(e)
                graph[X][A].add(label)

    return translate_graph()


def label_of(edge: pgv.Edge) -> str:
    return edge.attr["label"]


def filter_ε_transitions(edges: Iterator[pgv.Edge]) -> Iterator[pgv.Edge]:
    return (e for e in edges if label_of(e) != EPSILON)
