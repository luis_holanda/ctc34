import pygraphviz as pgv

def main():
    for i in range(1, 5):
        a = pgv.AGraph(f"teste{i}.dot")
        a.layout("circo")
        a.draw(f"teste{i}.png")


if __name__ == '__main__':
    main()
