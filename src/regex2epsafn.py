from typing import Tuple, Set
from src.regexhelpers import check_alphabet, check_parenthesis
import src.regexparser as parser
import pygraphviz as pgv

def run(seq: str) -> Tuple[pgv.AGraph, Set[str]]:
    if not check_alphabet(seq):
        raise ValueError()
    if not check_parenthesis(seq):
        raise ValueError()
    parsed = parser.parse(seq)
    return parsed.gen_visual_graph()